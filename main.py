import argparse

from sample_apps.init_apps import init_apps

if __name__ == "__main__":
    init_apps()
