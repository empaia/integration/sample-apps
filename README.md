# Sample Apps

Contains valid and invalid sample apps and their EAD for use in other projects, e.g. as submodules. Possible use cases:

* For automated tests
* Used in tutorial and tests of the EMPAIA App Test Suite (EATS)

## Code Checks

* always format code with `black` and `isort`
* check code quality using `pylint`
    * `black` formatting should resolve most issues for `pycodestyle`
    * `E203` warning for slice operators are ignored by `black` formatting since it is not PEP 8 compliant (see [here](https://github.com/psf/black/blob/main/docs/guides/using_black_with_other_tools.md#pycodestyle))

```bash
sudo apt update
sudo apt install python3-venv python3-pip
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

```bash
black sample_apps
isort sample_apps
pycodestyle sample_apps
pylint sample_apps
```