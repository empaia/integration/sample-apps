import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_processing_data(my_wsi: dict, my_rectangles: dict):
    """
    get wsi rectangle dict
    """
    data = {}
    for wsi in my_wsis["items"]:
        data[wsi["id"]] = get_rectangles_for_wsi(my_rectangles, wsi["id"])
    return data


def get_rectangles_for_wsi(my_rectangles: dict, wsi_id: str):
    """
    get rectangle collection with rectangles referencing given wsi_id
    """
    for rectangle_collection in my_rectangles["items"]:
        if rectangle_collection["reference_id"] == wsi_id:
            return rectangle_collection


def get_threshold_for_rectangle(my_thresholds: dict, rectangle_id: str):
    """
    get threshold for given rectangle_id
    """
    thresholds = {}
    for threshold_collection in my_thresholds["items"]:
        for threshold in threshold_collection["items"]:
            if threshold["reference_id"] == rectangle_id:
                return threshold["value"]
    return None


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(wsi_id: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    level = 0

    tile_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def count_tumor_cells(wsi_tile: Image, threshold: float):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return int(threshold * 100)


def compute_avg(tumor_cell_counts: dict):
    """
    pretends to do something useful

    Parameters:
        tumor_cell_counts: contains list of items with numeric value
    """
    values = [item["value"] for item in tumor_cell_counts["items"]]
    return sum(values) / len(values)


my_wsis = get_input("my_wsis")
my_rectangles = get_input("my_rectangles")
my_thresholds = get_input("my_thresholds")

tumor_cell_counts = {"item_type": "collection", "items": []}
avg_tumor_cell_counts = {"item_type": "float", "items": []}

data = get_processing_data(my_wsis, my_rectangles)

for wsi_id in data:
    rectangles = data[wsi_id]["items"]

    wsi_tumor_cell_counts = {"item_type": "integer", "items": []}

    for rectangle in rectangles:
        wsi_tile = get_wsi_tile(wsi_id, rectangle)

        tumor_cell_count = {
            "name": "cell count tumor",  # choose name freely
            "type": "integer",
            "value": count_tumor_cells(wsi_tile, get_threshold_for_rectangle(my_thresholds, rectangle["id"])),
            "reference_id": rectangle["id"],  # set reference to rectangle
            "reference_type": "annotation",
        }
        wsi_tumor_cell_counts["items"].append(tumor_cell_count)

    wsi_avg_tumor_cell_count = {
        "name": "avg tumor cell count",
        "type": "float",
        "value": compute_avg(wsi_tumor_cell_counts),
        "reference_id": data[wsi_id]["id"],  # set reference to rectangle collection
        "reference_type": "collection",
    }

    tumor_cell_counts["items"].append(wsi_tumor_cell_counts)
    avg_tumor_cell_counts["items"].append(wsi_avg_tumor_cell_count)

post_output("tumor_cell_counts", tumor_cell_counts)
post_output("avg_tumor_cell_counts", avg_tumor_cell_counts)

put_finalize()
