import os
from io import BytesIO
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def get_configuration():
    """
    gets configuration parameter by key

    Parameters:
        key: key (name) of the configuration parameter
    """
    url = f"{APP_API}/v0/{JOB_ID}/configuration"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def count_tumor_cells_external_api(wsi_tile: Image, user: str, password: str, parameter: int):
    """
    pretends to do request external api

    Parameters:
        wsi_tile: WSI image tile
        user: user for external api
        password: password for external apiwtte
        parameter: some parameter
    """
    return 42


my_wsi = get_input("my_wsi")
my_rectangle = get_input("my_rectangle")
wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

configuration = get_configuration()
user = configuration["private_api_username"]
password = configuration["private_api_password"]
parameter = configuration.get("optional_parameter", 84)

tumor_cell_count = {
    "name": "cell count cumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells_external_api(wsi_tile, user, password, parameter),
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
