from setuptools import setup

setup(
    name="App",
    version="1.0.0",
    author="Theodore Evans",
    author_email="theodore.evans@dai-labor.de",
    license="MIT",
    packages=["app"],
    install_requires=[
        "numpy",
        "pillow",
        "requests",
    ],
)
