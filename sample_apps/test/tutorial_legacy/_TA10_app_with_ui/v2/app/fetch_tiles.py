from typing import Any, Callable, Collection, Dict

from PIL import Image

_rectangle = Dict[str, Any]


def fetch_tiles(
    get_tile: Callable[[_rectangle], Image.Image], region: _rectangle, max_tile_size: int
) -> Collection[Image.Image]:
    start_x, start_y = region["upper_left"]
    region_width, region_height = region["width"], region["height"]

    columns_whole = region_width // max_tile_size
    columns_remainder = region_width % max_tile_size

    rows_whole = region_height // max_tile_size
    rows_remainder = region_height % max_tile_size

    tile_images = []
    tile_rectangles = []

    for j in range(rows_whole + (rows_remainder > 0)):
        for i in range(columns_whole + (columns_remainder > 0)):
            x, y = (start_x + i * max_tile_size, start_y + j * max_tile_size)

            width = min(x + max_tile_size, start_x + region_width) - x
            height = min(y + max_tile_size, start_y + region_height) - y

            rectangle = {"upper_left": [x, y], "width": width, "height": height}

            image = get_tile(rectangle)

            tile_images.append(image)
            tile_rectangles.append(rectangle)

    return tile_images, tile_rectangles
