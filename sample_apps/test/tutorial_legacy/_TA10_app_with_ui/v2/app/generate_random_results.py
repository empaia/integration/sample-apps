from typing import List

import numpy as np


def get_random_points(rectangle: dict, density: float) -> List[List]:
    """
    Generates point coordinates in wsi coordinate system using Poisson disc sampling

    Parameters:
        rectangle: dictionary defining regions over which to generate points
        spacing: approximate minimum spacing of points in pixels
    """
    width = rectangle["width"]
    height = rectangle["height"]

    n_points = round(width * height * density / 1000)

    x_min = rectangle["upper_left"][0]
    y_min = rectangle["upper_left"][1]

    sample_array = np.random.rand(n_points, 2)

    points = []

    for sample in sample_array.tolist():
        x = round(x_min + sample[0] * width)
        y = round(y_min + sample[1] * height)
        points.append([x, y])

    return points


def get_random_confidence(beta: float = 20) -> float:
    """
    Generates a random confidence score in [0,1), with most outputs being close to 0 or 1

    Parameters:
        beta: controls the sharpness of the step between high and low confidence values
    """
    x = np.random.rand() - 0.5
    return 1 / (1 + np.exp(-x * beta))
