import argparse
import functools
import logging
from typing import List, Tuple

from app.api_interface import ApiInterface
from app.fetch_tiles import fetch_tiles
from app.generate_random_results import get_random_confidence, get_random_points

MAX_TILE_SIZE = 5000
POINT_DENSITY = 0.5
CLASSIFICATION_THRESHOLD = 0.7
BATCH_SIZE = 5000
POS_CLASS = "org.empaia.vendor_name.tutorial_app_10.v2.classes.pos"
NEG_CLASS = "org.empaia.vendor_name.tutorial_app_10.v2.classes.neg"


def detect_nuclei(slide: dict, roi: dict, density: float) -> Tuple[dict, dict]:
    """
    pretends to detect nuclei

    Parameters:
        slide: contains WSI id (and meta data)
        roi: tile position on level 0
    """

    nuclei = []
    confidences = []

    points = get_random_points(roi, density)

    slide_npp = slide["pixel_size_nm"]["x"]

    for point in points:
        nucleus = {
            "name": "nucleus",
            "type": "point",
            "reference_id": slide["id"],
            "reference_type": "wsi",
            "coordinates": point,
            "npp_created": slide_npp,
            "npp_viewing": [
                slide_npp,
                slide_npp * 2**2,
            ],
        }
        nuclei.append(nucleus)

        confidence_value = get_random_confidence()

        confidence = {
            "name": "confidence score",
            "type": "float",
            "value": confidence_value,
            "reference_id": None,
            "reference_type": "annotation",
        }
        confidences.append(confidence)

    return nuclei, confidences


def classify_nuclei(confidences: dict, positive_class: str, negative_class: str, threshold: float = 0.5):
    """
    classifies nuclei based on Value for each item in a collection of float values

    Parameters:
        confidences: a collection of float values for model confidence referencing corresponding annotations
        positive_class: class assigned for values > threshold
        negative_class: class assigned for values <= threshold
        threshold: threshold for positive/negative classifications, default: 0.5
    """
    num_pos = num_neg = 0
    classifications = []
    for confidence in confidences:
        if confidence["value"] > threshold:
            value = positive_class
            num_pos += 1
        else:
            value = negative_class
            num_neg += 1
        classification = {
            "value": value,
            "reference_id": confidence["reference_id"],
            "reference_type": "annotation",
        }

        classifications.append(classification)

    return classifications, num_pos, num_neg


def main(verbosity: int):
    log_level = max(logging.WARN - 10 * verbosity, logging.DEBUG)
    api = ApiInterface(log_level=log_level)

    wsi_slide = api.get_input("slide")
    region_of_interest = api.get_input("region_of_interest")

    get_tile_from_slide = functools.partial(api.get_wsi_tile, wsi_slide=wsi_slide)
    tiles, rectangles = fetch_tiles(get_tile_from_slide, region_of_interest, max_tile_size=MAX_TILE_SIZE)
    _, _ = tiles, rectangles  # not used in sample app

    nuclei_collection = {
        "item_type": "point",
        "reference_id": region_of_interest["id"],
        "reference_type": "annotation",
        "items": [],
    }

    confidence_collection = {"item_type": "float", "items": []}

    classification_collection = {"item_type": "class", "items": []}

    nuclei_collection = api.post_output("detected_nuclei", nuclei_collection)
    confidence_collection = api.post_output("model_confidences", confidence_collection)
    classification_collection = api.post_output("nucleus_classifications", classification_collection)

    nuclei, confidences = detect_nuclei(wsi_slide, region_of_interest, density=POINT_DENSITY)
    classifications, number_positive, number_negative = classify_nuclei(
        confidences, POS_CLASS, NEG_CLASS, threshold=CLASSIFICATION_THRESHOLD
    )

    batch_size = BATCH_SIZE
    start_idx = 0
    end_idx = start_idx + batch_size
    while start_idx < len(nuclei):
        nuclei = api.post_items_to_collection(nuclei_collection["id"], nuclei[start_idx:end_idx])

        for nucleus, confidence, classification in zip(
            nuclei["items"], confidences[start_idx:end_idx], classifications[start_idx:end_idx]
        ):
            classification["reference_id"] = confidence["reference_id"] = nucleus["id"]

        api.post_items_to_collection(confidence_collection["id"], confidences[start_idx:end_idx])
        api.post_items_to_collection(classification_collection["id"], classifications[start_idx:end_idx])
        start_idx += batch_size
        end_idx = start_idx + batch_size

    positivity = number_positive / (number_positive + number_negative)

    numerical_outputs = {
        "number_positive": number_positive,
        "number_negative": number_negative,
        "positivity": round(positivity, 3),
    }

    for key, value in numerical_outputs.items():
        data = {
            "name": key.replace("_", " "),
            "value": value,
            "type": "integer" if isinstance(value, int) else "float",
            "reference_id": region_of_interest["id"],
            "reference_type": "annotation",
        }
        api.post_output(key, data)

    api.put_finalize()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", help="increase logging verbosity", action="count", default=0)
    args = parser.parse_args()

    main(verbosity=args.verbose)
