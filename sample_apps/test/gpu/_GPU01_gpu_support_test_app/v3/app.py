import os
import subprocess

import requests

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_gpu_devices():
    output = subprocess.run(["nvidia-smi", "-L"], capture_output=True, check=True)
    gpu_stats = output.stdout.decode().split("\n")
    detected = len(gpu_stats) > 0
    device = gpu_stats[0]
    return detected, device


my_wsi = get_input("my_wsi")

gpu_devices_found, gpu_device_detected = get_gpu_devices()

gpu_detected = {
    "name": "GPU detected",
    "type": "bool",
    "value": gpu_devices_found,
    "reference_type": "wsi",
    "reference_id": my_wsi["id"],
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("gpu_detected", gpu_detected)

gpu_device = {
    "name": "GPU device",
    "type": "string",
    "value": gpu_device_detected,
    "reference_type": "wsi",
    "reference_id": my_wsi["id"],
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("gpu_device", gpu_device)

put_finalize()
