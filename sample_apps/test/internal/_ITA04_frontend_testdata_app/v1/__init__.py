import json
from pathlib import Path

path = Path(__file__).parent
path = Path(f"{path}/ead.json")
ead = json.loads(path.read_bytes())

namespace_normalized_tag = ead["namespace"]
_idx = namespace_normalized_tag.rfind(".")
namespace_normalized_tag = namespace_normalized_tag[:_idx] + ":" + namespace_normalized_tag[_idx + 1 :]
namespace_normalized_tag = namespace_normalized_tag.replace(".", "-")
registry = f"registry.gitlab.com/empaia/integration/sample-apps/v1/{namespace_normalized_tag}"
