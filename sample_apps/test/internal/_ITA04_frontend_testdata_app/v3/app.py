import os
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def detect_hexagons(my_wsi: dict, start_x, start_y):
    """
    generates hexagons

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    polygons_batch = []
    for i in range(123):
        start_x += i * 100
        start_y += i * 50
        coords = [start_x, start_y]
        coords.append([start_x + 50, start_y + 50])
        coords.append([start_x + 100, start_y + 50])
        coords.append([start_x + 150, start_y + 0])
        coords.append([start_x + 100, start_y - 50])
        coords.append([start_x + 50, start_y - 50])
        poly = {
            "name": f"polygon_{i}",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": coords,
            "npp_created": 499,
            # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],
            # (optional) recommended pixel reslution range for viewer to display annotation,
            # if npp_created is not sufficient
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        polygons_batch.append(poly)
    return polygons_batch


# input
my_wsi = get_input("my_wsi")

# output
items = []
for i in range(10):
    item = {
        "name": f"rectangle_{i}",
        "type": "rectangle",
        "reference_id": my_wsi["id"],  # each point annotation references my_wsi
        "reference_type": "wsi",
        "upper_left": [i * 500, i * 500],  # Always use WSI base level coordinates
        "width": i * 500,
        "height": i * 500,
        "npp_created": 499,
        # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
        "npp_viewing": [
            4990,
            399200,
        ],
        # (optional) recommended pixel reslution range for viewer to display annotation,
        # if npp_created is not sufficient
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }
    items.append(item)
rectangles = {
    "item_type": "rectangle",
    "items": items,
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}
rectangles = post_output("rectangles", rectangles)

polygons = {
    "item_type": "collection",
    "items": [],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}
polygons = post_output("polygons", polygons)
for rectangle in rectangles["items"]:
    polygons_inner = [
        {
            "item_type": "polygon",
            "items": [],
            "reference_id": rectangle["id"],
            "reference_type": "annotation",
            "type": "collection",  # NEW required in v3 apps
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
    ]
    polygons_inner = post_items_to_collection(polygons["id"], polygons_inner)
    inner_collection_id = polygons_inner["items"][0]["id"]
    start_x = rectangle["upper_left"][0]
    start_y = rectangle["upper_left"][1]
    items = detect_hexagons(my_wsi, start_x, start_y)
    post_items_to_collection(inner_collection_id, items)


put_finalize()
