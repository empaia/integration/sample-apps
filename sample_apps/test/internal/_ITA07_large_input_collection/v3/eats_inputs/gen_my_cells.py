import json
import pathlib
from uuid import uuid4

current_dir = pathlib.Path(__file__).parent.absolute()
my_cells = pathlib.Path(current_dir, "my_cells.json")

data = {
    "id": str(uuid4()),
    "item_type": "collection",
    "items": [
        {
            "id": str(uuid4()),
            "item_type": "collection",
            "items": [
                {
                    "id": str(uuid4()),
                    "item_type": "collection",
                    "items": [
                        {"id": str(uuid4()), "item_type": "point", "items": []},
                    ],
                },
            ],
        },
        {
            "id": str(uuid4()),
            "item_type": "collection",
            "items": [
                {
                    "id": str(uuid4()),
                    "item_type": "collection",
                    "items": [
                        {"id": str(uuid4()), "item_type": "point", "items": []},
                    ],
                },
            ],
        },
    ],
}

for i in range(2):
    for j in range(20000):
        point = {
            "id": str(uuid4()),
            "type": "point",
            "name": "cell",
            "coordinates": [1000, 2000],
            "reference_id": "37bd11b8-3995-4377-bf57-e718e797d515",
            "reference_type": "wsi",
            "npp_created": 499,
            "npp_viewing": [1, 499123],
        }
        data["items"][i]["items"][0]["items"][0]["items"].append(point)


with my_cells.open("w", encoding="UTF-8") as f:
    json.dump(data, f)
