import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def count_tumor_cells(wsi_tile: Image):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return 42


single_wsi = get_input("single_wsi")
single_rectangle = get_input("single_rectangle")
wsi_tile = get_wsi_tile(single_wsi, single_rectangle)

collection_wsi = get_input("collection_wsi")
collection_wsi = get_input("collection_wsi_no_ids")
collection_collection_wsi = get_input("collection_collection_wsi")
single_point = get_input("single_point")
single_line = get_input("single_line")
single_arrow = get_input("single_arrow")
single_circle = get_input("single_circle")
single_polygon = get_input("single_polygon")
collection_rectangle = get_input("collection_rectangle")
collection_point = get_input("collection_point")
collection_line = get_input("collection_line")
collection_arrow = get_input("collection_arrow")
collection_circle = get_input("collection_circle")
collection_polygon = get_input("collection_polygon")
single_integer = get_input("single_integer")
single_float = get_input("single_float")
single_string = get_input("single_string")
single_bool = get_input("single_bool")
collection_integer = get_input("collection_integer")
collection_float = get_input("collection_float")
collection_string = get_input("collection_string")
collection_bool = get_input("collection_bool")

tumor_cell_count = {
    "name": "cell count cumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells(wsi_tile),
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
