import os
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


my_wsi = get_input("my_wsi")

my_cells = {
    "item_type": "collection",
    "items": [
        {
            "item_type": "point",
            "items": [],
            "type": "collection",  # NEW required in v3 apps
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        },
        {
            "item_type": "point",
            "items": [],
            "type": "collection",  # NEW required in v3 apps
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        },
    ],
    "type": "collection",  # NEW required in v3 apps
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}
my_cells = post_output("my_cells", my_cells)

# posts 10k cells in 1k batches to collection
for nested_collection in my_cells["items"]:
    for i in range(10):
        items_batch = []
        for j in range(1000):
            cell = {
                "name": "cell",
                "type": "point",
                "reference_id": my_wsi["id"],  # each point annotation references my_wsi
                "reference_type": "wsi",
                "coordinates": [0 + i, 0 + j],  # Always use WSI base level coordinates
                "npp_created": 499,
                # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
                "npp_viewing": [
                    499,
                    3992,
                ],
                # (optional) recommended pixel reslution range for viewer to display annotation,
                # if npp_created is not sufficient
                "creator_type": "job",  # NEW required in v3 apps
                "creator_id": JOB_ID,  # NEW required in v3 apps
            }
            items_batch.append(cell)

        post_items_to_collection(nested_collection["id"], items_batch)

put_finalize()
