# TEST
# ASUMES WSI:
# OpenSlide_adapted/Fluorescence OME-Tif

import os
from io import BytesIO

import requests

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def test_fluorescence_functionality(my_wsi: dict):
    wsi_id = my_wsi["id"]
    level = 0
    x = 13
    y = 7
    width = 1000
    height = 1000
    print(my_wsi)
    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    params = {
        "image_channels": 0,
        "image_format": "tiff",
        "image_quality": 90,
        "z": 0,
    }
    r = requests.get(tile_url, headers=HEADERS, params=params)
    assert len(r.content) == 2435
    r.raise_for_status()


my_wsi = get_input("my_wsi")
test_fluorescence_functionality(my_wsi)

dummy_out = {
    "name": "cell count cumor",  # choose name freely
    "type": "integer",
    "value": 43,
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("dummy_out", dummy_out)

put_finalize()
