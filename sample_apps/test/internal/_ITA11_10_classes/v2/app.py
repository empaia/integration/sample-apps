import os
import random
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    cells = {
        "item_type": "point",
        "items": [],
        "reference_id": my_rectangle["id"],  # point_annotations collection references my_rectangle
        "reference_type": "annotation",
    }
    cell_classes = {"item_type": "class", "items": []}
    # your computational code below
    for i in range(100):
        coord_x = random.randint(
            my_rectangle["upper_left"][0],
            my_rectangle["width"] + my_rectangle["upper_left"][0],
        )
        coord_y = random.randint(
            my_rectangle["upper_left"][1],
            my_rectangle["height"] + my_rectangle["upper_left"][1],
        )
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [coord_x, coord_y],  # Always use WSI base level coordinates
            "npp_created": 499,
            # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],
            # (optional) recommended pixel reslution range for viewer to display annotation,
            # if npp_created is not sufficient
        }
        cells["items"].append(cell)

        classes = [f"org.empaia.vendor_name.internal_test_app_11.v2.classes.class_{i + 1}" for i in range(10)]
        cell_class = {
            "value": classes[i // 10],
            "reference_id": None,  # yet unknown
            "reference_type": "annotation",
        }
        cell_classes["items"].append(cell_class)

    return cells, cell_classes


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = {"item_type": "collection", "items": []}
my_cell_classes = {"item_type": "collection", "items": []}

for my_rectangle in my_rectangles["items"]:
    cells, cell_classes = detect_cells(my_wsi, my_rectangle)
    my_cells["items"].append(cells)
    my_cell_classes["items"].append(cell_classes)

my_cells = post_output("my_cells", my_cells)  # response includes IDs in addition to the original data

for cells, cell_classes in zip(my_cells["items"], my_cell_classes["items"]):
    for cell, cell_class in zip(cells["items"], cell_classes["items"]):
        cell_class["reference_id"] = cell["id"]  # id from POST response

post_output("my_cell_classes", my_cell_classes)

put_finalize()
