import os
from io import BytesIO
from time import sleep

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, upper_left: dict, width: int, height: int):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        upper_left: upper left coordinate of the tile
        width: width of the tile
        height: height of the tile
    """
    x = upper_left[0]
    y = upper_left[1]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


mode = get_app_mode()["mode"]
my_wsi = get_input("my_wsi")

my_string = {
    "name": "my_string",  # choose name freely
    "value": "...",
    "type": "string",
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}
if mode == "STANDALONE":
    my_string["value"] = mode
elif mode == "PREPROCESSING":
    my_string["value"] = mode

my_point = {
    "name": "cell",
    "type": "point",
    "reference_id": my_wsi["id"],  # each point annotation references my_wsi
    "reference_type": "wsi",
    "coordinates": [1337, 1337],  # Always use WSI base level coordinates
    "npp_created": 499,
    # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
    "npp_viewing": [
        499,
        3992,
    ],
    # (optional) recommended pixel reslution range for viewer to display annotation,
    # if npp_created is not sufficient
    "creator_type": "job",  # NEW required in v3 apps
    "creator_id": JOB_ID,  # NEW required in v3 apps
}

post_output("my_string", my_string)
post_output("my_point", my_point)

# for testing purpose
sleep(10)

put_finalize()
