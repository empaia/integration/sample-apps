import json
from pathlib import Path

path = Path(__file__).parent
path = Path(f"{path}/ead.json")
ead = json.loads(path.read_bytes())

namespace_normalized_tag = ead["namespace"]
_idx = namespace_normalized_tag.rfind(".")
namespace_normalized = namespace_normalized_tag[: _idx - 3]
version = namespace_normalized_tag[_idx - 2 :]
namespace_normalized = namespace_normalized.replace(".", "-")
namespace_normalized_tag = f"{namespace_normalized}:{version}"
registry = f"registry.gitlab.com/empaia/integration/sample-apps/v3/{namespace_normalized_tag}"
