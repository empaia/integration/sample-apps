import json
import os
from io import BytesIO
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_configuration():
    """
    gets configuration parameter by key

    Parameters:
        key: key (name) of the configuration parameter
    """
    url = f"{APP_API}/v0/{JOB_ID}/configuration"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


configuration = get_configuration()
user = configuration["private_api_username"]
password = configuration["private_api_password"]
parameter = configuration.get("optional_parameter", 84)
escaped_json = configuration.get("escaped_json")
assert escaped_json == '{"CEN-17": "cen17"}'
escaped_json = json.loads(escaped_json)
assert escaped_json["CEN-17"] == "cen17"

tumor_cell_count = {
    "name": "cell count cumor",  # choose name freely
    "type": "integer",
    "value": 42,
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
