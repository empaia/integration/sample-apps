import os
from typing import List

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


# input
my_wsi = get_input("my_wsi")

# output
items = []
for i in range(100):
    item = {
        "name": f"point_{i}",
        "type": "point",
        "reference_id": my_wsi["id"],  # each point annotation references my_wsi
        "reference_type": "wsi",
        "coordinates": [i, i],  # Always use WSI base level coordinates
        "npp_created": 499,
        # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
        "npp_viewing": [
            499,
            3992,
        ],
        # (optional) recommended pixel reslution range for viewer to display annotation,
        # if npp_created is not sufficient
    }
    items.append(item)
points = {"item_type": "point", "items": items}
points = post_output("points", points)

items = []
for i in range(10):
    item = {
        "name": f"circle_{i}",
        "type": "circle",
        "reference_id": my_wsi["id"],  # each point annotation references my_wsi
        "reference_type": "wsi",
        "center": [i * 100, i * 100],  # Always use WSI base level coordinates
        "radius": i * 100,
        "npp_created": 499,
        # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
        "npp_viewing": [
            499,
            39920,
        ],
        # (optional) recommended pixel reslution range for viewer to display annotation,
        # if npp_created is not sufficient
    }
    items.append(item)
circles = {"item_type": "circle", "items": items}
circles = post_output("circles", circles)

items = []
for i in range(10):
    item = {
        "name": f"rectangle_{i}",
        "type": "rectangle",
        "reference_id": my_wsi["id"],  # each point annotation references my_wsi
        "reference_type": "wsi",
        "upper_left": [i * 500, i * 500],  # Always use WSI base level coordinates
        "width": i * 500,
        "height": i * 500,
        "npp_created": 499,
        # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
        "npp_viewing": [
            4990,
            399200,
        ],
        # (optional) recommended pixel reslution range for viewer to display annotation,
        # if npp_created is not sufficient
    }
    items.append(item)
rectangles = {"item_type": "rectangle", "items": items}
rectangles = post_output("rectangles", rectangles)

put_finalize()
