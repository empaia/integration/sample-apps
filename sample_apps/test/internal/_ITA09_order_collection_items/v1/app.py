import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


my_wsi = get_input("my_wsi")

my_cells = {
    "item_type": "collection",
    "items": [
        {
            "item_type": "point",
            "items": [],
        },
        {
            "item_type": "point",
            "items": [],
        },
        {
            "item_type": "point",
            "items": [],
        },
        {
            "item_type": "point",
            "items": [],
        },
    ],
}

for i in range(len(my_cells["items"])):
    for j in range(20):
        my_cells["items"][i]["items"].append(
            {
                "name": f"Cell_{i}{j}",
                "type": "point",
                "reference_id": "37bd11b8-3995-4377-bf57-e718e797d515",
                "reference_type": "wsi",
                "npp_created": 499,
                "npp_viewing": [499, 3992],
                "coordinates": [123, 456],
            },
        )

result = post_output("my_cells", my_cells)

for i in range(len(my_cells["items"])):
    for j in range(len(my_cells["items"][i])):
        payload_item = my_cells["items"][i]["items"][j]
        response_item = result["items"][i]["items"][j]
        assert payload_item["name"] == response_item["name"]

put_finalize()
