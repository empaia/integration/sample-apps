import os
import time

job_id = os.getenv("EMPAIA_JOB_ID")
app_service = os.getenv("EMPAIA_APP_API")
access_token = os.getenv("EMPAIA_TOKEN")

print(f"EMPAIA_JOB_ID  {job_id}")
print(f"EMPAIA_APP_API {app_service}")
print(f"EMPAIA_TOKEN   {access_token}")

t, *rest = app_service.split()
time.sleep(int(t))
if rest:
    raise Exception(" ".join(rest))
else:
    print("completed")
