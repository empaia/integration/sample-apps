# test-app

Super-simple app for testing the job-execution-service. The app does nothing and
does not interact with the app-service, it just checks its parameters (job-id,
app-service-url and access-token) and behaves differently based on those inputs.

The App will simply print its parameters to stdout, then try to `sleep` for the
number of seconds provided in the first token of the `EMPAIA_APP_API` environment
variable. If the variable contains any more tokens, the App will then raise an
exception and thus fail, otherwise it completes normally.

## Sample requests for Job Execution Service

* run for 3 seconds, then complete normally
  ```
  {
    "app_id": "test-app",
    "job_id": "cf0c35e9-7bee-4fc9-a5f2-9ac8e7fbbe30",
    "access_token": "doesnotmatter",
    "app_service_url": "3"
  }
  ```

* run for 3 seconds, then fail
  ```
  {
    "app_id": "test-app",
    "job_id": "cf0c35e9-7bee-4fc9-a5f2-9ac8e7fbbe30",
    "access_token": "doesnotmatter",
    "app_service_url": "3 and then fail"
  }
  ```

* timeout
  ```
  {
    "app_id": "test-app",
    "job_id": "cf0c35e9-7bee-4fc9-a5f2-9ac8e7fbbe30",
    "access_token": "doesnotmatter",
    "app_service_url": "3",
    "timeout": 2
  }
  ```

Assuming the App has been built as `test-app`, i.e.
```
docker build -t test-app .
```

The above requests correspond to the following Docker command, but running the
App directly does not make much sense, except for testing the above behaviour.
```
docker run -e EMPAIA_JOB_ID={job_id} \
           -e EMPAIA_TOKEN={access_token} \
           -e EMPAIA_APP_API={app_service} \
           test-app
```

The test-app is also already deployed to Docker-Hub and can be used from there,
without bulding it locally, by replacing the `test-app` in the above examples
with `dailab/test-app`.