import math
import os
import random
from typing import List, Union

import requests
from shapely.geometry import Polygon

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

TUMOR_CLASS = "org.empaia.vendor_name.development_app_05.v3.0.classes.tumor"
NON_TUMOR_CLASS = "org.empaia.vendor_name.development_app_05.v3.0.classes.non_tumor"
TILE_SIZE = 512

# initialize random with contant
random.seed(0)


class CustomAppException(Exception):
    pass


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_input_collection(key: str):
    """
    get input collection by key as defined in EAD with all items
    """
    # get shallow collection
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow=true"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    collection = r.json()
    collection_id = collection["id"]

    item_count = collection["item_count"]

    # return collection if empty
    if item_count == 0:
        return collection

    # get all items
    batch_size = 1000
    batches = math.ceil(item_count / batch_size)

    item_url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/query"

    items = []
    for i in range(batches):
        skip = i * batch_size
        params = {"skip": skip, "limit": batch_size}
        r = requests.put(item_url, json={}, params=params, headers=HEADERS)
        r.raise_for_status()
        items.extend(r.json()["items"])

    collection["items"] = items
    return collection


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"
    data = {"user_message": str(error)}
    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def generate_output(rects: List[Polygon], my_wsi: dict, my_regions: dict):
    """
    generate cells and cell classes (and ratios in STANDALONE mode)
    """
    npp_created = my_wsi["pixel_size_nm"]["x"]
    cells = []
    cell_classes = []
    region_ratios = []
    average_ratio = None

    for idx, rect in enumerate(rects):
        minx, miny, maxx, maxy = rect.bounds

        rect_classes = []
        tumor_count = 0
        class_count = 0

        generate_cell_count = int(math.sqrt((maxx - minx) + (maxy - miny)))

        for i in range(generate_cell_count):
            center_x = random.randint(int(minx), int(maxx))
            center_y = random.randint(int(miny), int(maxy))
            coordinates = [
                [center_x - 25, center_y - 25],
                [center_x + 25, center_y - 25],
                [center_x + 25, center_y + 25],
                [center_x - 25, center_y + 25],
            ]

            polygon = {
                "name": "Cell",
                "type": "polygon",
                "reference_id": my_wsi["id"],
                "reference_type": "wsi",
                "coordinates": coordinates,
                "npp_created": npp_created,
                "npp_viewing": [
                    npp_created,
                    npp_created * 10,
                ],
                "creator_type": "job",
                "creator_id": JOB_ID,
            }
            cells.append(polygon)

            value = TUMOR_CLASS if i % 2 == 0 else NON_TUMOR_CLASS

            if value == TUMOR_CLASS:
                tumor_count += 1

            cell_class = {
                "value": value,
                "reference_id": None,
                "reference_type": "annotation",
                "type": "class",
                "creator_type": "job",
                "creator_id": JOB_ID,
            }
            rect_classes.append(cell_class)

            class_count += 1

        if my_regions:
            region_ratio = {
                "name": "Ratio",
                "description": "Ratio",
                "reference_id": my_regions["items"][idx]["id"],
                "reference_type": "annotation",
                "type": "float",
                "value": tumor_count / class_count,
                "creator_type": "job",
                "creator_id": JOB_ID,
            }

            region_ratios.append(region_ratio)

        cell_classes.extend(rect_classes)

    if my_regions:
        sum_ratio = 0.0
        for ratio in region_ratios:
            sum_ratio += ratio["value"]

        average_ratio = {
            "name": "Avg Ratio",
            "description": "Avg Ratio",
            "reference_id": my_wsi["id"],
            "reference_type": "wsi",
            "type": "float",
            "value": sum_ratio / len(region_ratios),
            "creator_type": "job",
            "creator_id": JOB_ID,
        }

    return cells, cell_classes, region_ratios, average_ratio


def get_empty_collection(item_type: str):
    return {
        "item_type": item_type,
        "items": [],
        "type": "collection",
        "creator_type": "job",
        "creator_id": JOB_ID,
    }


# mode checking; app only supports STANDALONE
mode = get_app_mode()["mode"]
if mode != "STANDALONE":
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = get_empty_collection(item_type="polygon")
my_cells = post_output("my_cells", my_cells)

my_cell_classes = get_empty_collection(item_type="class")
my_cell_classes = post_output("my_cell_classes", my_cell_classes)

tumor_ratios = get_empty_collection(item_type="float")
tumor_ratios = post_output("tumor_ratios", tumor_ratios)

regions = []
for region in my_rectangles["items"]:
    x_min = region["upper_left"][0]
    x_max = region["upper_left"][0] + region["width"]
    y_min = region["upper_left"][1]
    y_max = region["upper_left"][1] + region["height"]
    # failure if area too small
    if x_max - x_min < TILE_SIZE or y_max - y_min < TILE_SIZE:
        error_msg = (
            "Selected region was too small. Please select a region with at "
            "least 512*512 pixels on highest resolution."
        )
        put_failure(error_msg)
        raise CustomAppException(error_msg)

    rect = Polygon(
        [
            (x_min, y_min),
            (x_max, y_min),
            (x_max, y_max),
            (x_min, y_max),
        ]
    )
    regions.append(rect)

cells, cell_classes, region_ratios, avg_ratio = generate_output(regions, my_wsi, my_rectangles)

# extend output collections and post additional output
if len(cells) > 0:
    cell_items_with_ids = post_items_to_collection(my_cells["id"], cells)

    for i in range(len(cell_items_with_ids["items"])):
        cell_classes[i]["reference_id"] = cell_items_with_ids["items"][i]["id"]

    post_items_to_collection(my_cell_classes["id"], cell_classes)

    post_items_to_collection(tumor_ratios["id"], region_ratios)
    post_output("aggregated_tumor_ratio", avg_ratio)


put_finalize()
