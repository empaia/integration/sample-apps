import math
import os
import random
from io import BytesIO
from typing import List, Union

import numpy as np
import requests
from PIL import Image
from scipy.ndimage import binary_fill_holes
from shapely.affinity import scale
from shapely.geometry import Polygon
from skimage.color import rgb2gray
from skimage.filters.thresholding import threshold_otsu
from skimage.measure import find_contours

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

TUMOR_CLASS = "org.empaia.vendor_name.development_app_04.v3.0.classes.tumor"
NON_TUMOR_CLASS = "org.empaia.vendor_name.development_app_04.v3.0.classes.non_tumor"
TILE_SIZE = 512
MAX_GRID_SIZE = 1024
N_CELLS_PER_RECT = 25

# initialize random with contant
random.seed(0)


class CustomAppException(Exception):
    pass


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_input_collection(key: str):
    """
    get input collection by key as defined in EAD with all items
    """
    # get shallow collection
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow=true"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    collection = r.json()
    collection_id = collection["id"]

    item_count = collection["item_count"]

    # return collection if empty
    if item_count == 0:
        return collection

    # get all items
    batch_size = 1000
    batches = math.ceil(item_count / batch_size)

    item_url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/query"

    items = []
    for i in range(batches):
        skip = i * batch_size
        params = {"skip": skip, "limit": batch_size}
        r = requests.put(item_url, json={}, params=params, headers=HEADERS)
        r.raise_for_status()
        items.extend(r.json()["items"])

    collection["items"] = items
    return collection


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def get_overview(my_wsi: dict):
    """
    get overview image of slide.
    """
    wsi_id = my_wsi["id"]
    levels = my_wsi["levels"]
    for idx, level in enumerate(levels):
        x_dim = level["extent"]["x"]
        y_dim = level["extent"]["y"]
        if x_dim <= 5000 and y_dim <= 5000:
            scaling = level["downsample_factor"]
            url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{idx}/start/0/0/size/{x_dim}/{y_dim}"
            r = requests.get(url, headers=HEADERS)
            r.raise_for_status()
            return Image.open(BytesIO(r.content)), scaling
    return None, None


def get_tissue(overview: Image, scaling: float):
    """
    get tissue particles as polygons
    """
    np_overview = np.array(overview)
    gray_image = rgb2gray(np_overview)
    thresh = threshold_otsu(gray_image)
    binary = gray_image < thresh
    filled = binary_fill_holes(binary)
    filled = np.transpose(filled)
    contours = find_contours(filled)

    tissue_particles = []
    for contour in contours:
        if len(contour) > 250:
            particle = Polygon(contour)
            particle = scale(particle, xfact=scaling, yfact=scaling, origin=(0, 0))
            tissue_particles.append(particle)

    return tissue_particles


def generate_tissue_grid_rects(tissue_particles: List[Polygon]):
    """
    generate grid inside tissue particles
    """
    tissue_rects = []

    for particle in tissue_particles:
        minx, miny, maxx, maxy = particle.bounds

        x_range = maxx - minx
        y_range = maxy - miny
        grid_size = min(x_range / 10, y_range / 10, MAX_GRID_SIZE)

        posy = miny
        while posy < maxy:
            posx = minx
            while posx < maxx:
                rect = Polygon(
                    [
                        (posx, posy),
                        (posx + grid_size, posy),
                        (posx + grid_size, posy + grid_size),
                        (posx, posy + grid_size),
                    ]
                )
                if particle.covers(rect):
                    tissue_rects.append(rect)
                posx += grid_size
            posy += grid_size

    return tissue_rects


def generate_output(rects: List[Polygon], my_wsi: dict):
    """
    generate cells and cell confidences
    """
    npp_created = my_wsi["pixel_size_nm"]["x"]
    cells = []
    cell_confidences = []

    for rect in rects:
        minx, miny, maxx, maxy = rect.bounds

        for _ in range(N_CELLS_PER_RECT):
            point_x = random.randint(int(minx), int(maxx))
            point_y = random.randint(int(miny), int(maxy))

            point = {
                "name": "Cell",
                "type": "point",
                "reference_id": my_wsi["id"],
                "reference_type": "wsi",
                "coordinates": [point_x, point_y],
                "npp_created": npp_created,
                "npp_viewing": [
                    npp_created,
                    npp_created * 10,
                ],
                "creator_type": "job",
                "creator_id": JOB_ID,
            }
            cells.append(point)

            confidence = random.random()

            cell_confidence = {
                "name": "Cell Confidence",
                "value": confidence,
                "reference_id": None,
                "reference_type": "annotation",
                "type": "float",
                "creator_type": "job",
                "creator_id": JOB_ID,
            }
            cell_confidences.append(cell_confidence)

    return cells, cell_confidences


def get_cells_in_rectangle(my_rect: dict, input_cells: dict):
    """
    find cells from a given cell collection inside a rectangle

    Parameters:
        my_rect: the rectangle
        input_cells: collection of polygon annotations
    """

    cells_in_rect = []

    min_x = my_rect["upper_left"][0]
    max_x = my_rect["upper_left"][0] + my_rect["width"]
    min_y = my_rect["upper_left"][1]
    max_y = my_rect["upper_left"][1] + my_rect["height"]

    for cell in input_cells["items"]:
        centroid = cell["centroid"]

        if centroid[0] >= min_x and centroid[0] <= max_x and centroid[1] >= min_y and centroid[1] <= max_y:
            cells_in_rect.append(cell)

    return cells_in_rect


def generate_classes(cells: List[dict], cell_confidences: List[dict], confidence_threshold: dict):
    cell_ids = [str(cell["id"]) for cell in cells]

    classes = []
    for cell_confidence in cell_confidences:
        if str(cell_confidence["reference_id"]) in cell_ids:
            class_value = TUMOR_CLASS if cell_confidence["value"] >= confidence_threshold["value"] else NON_TUMOR_CLASS

            cell_class = {
                "value": class_value,
                "reference_id": cell_confidence["reference_id"],
                "reference_type": "annotation",
                "type": "class",
                "creator_type": "job",
                "creator_id": JOB_ID,
            }
            classes.append(cell_class)
    return classes


def get_empty_collection(item_type: str):
    return {
        "item_type": item_type,
        "items": [],
        "type": "collection",
        "creator_type": "job",
        "creator_id": JOB_ID,
    }


def preprocessing_mode():
    my_wsi = get_input("my_wsi")

    overview, scaling = get_overview(my_wsi)

    if not overview or not scaling:
        error_msg = "No tissue detected!"
        put_failure(error_msg)
        raise CustomAppException(error_msg)

    tissue_particles = get_tissue(overview, scaling)

    my_cells = get_empty_collection(item_type="point")
    my_cells = post_output("my_cells", my_cells)

    my_confidences = get_empty_collection(item_type="float")
    my_confidences = post_output("my_confidences", my_confidences)

    tissue_rects = generate_tissue_grid_rects(tissue_particles)
    cells, cell_confidences = generate_output(tissue_rects, my_wsi)

    # extend output collections
    if len(cells) > 0:
        cell_items_with_ids = post_items_to_collection(my_cells["id"], cells)

        for i in range(len(cell_items_with_ids["items"])):
            cell_confidences[i]["reference_id"] = cell_items_with_ids["items"][i]["id"]

        post_items_to_collection(my_confidences["id"], cell_confidences)


def postprocessing_mode():
    my_region = get_input("my_region")
    my_cells = get_input_collection("my_cells")
    my_confidences = get_input_collection("my_confidences")
    my_confidence_threshold = get_input("my_confidence_threshold")

    if my_confidence_threshold["value"] < 0.0 or my_confidence_threshold["value"] > 1.0:
        error_msg = "Threshold must be between 0.0 and 1.0"
        put_failure(error_msg)
        raise CustomAppException(error_msg)

    my_cell_classes = get_empty_collection(item_type="class")
    my_cell_classes = post_output("my_cell_classes", my_cell_classes)

    cells_in_region = get_cells_in_rectangle(my_region, my_cells)
    region_classes = generate_classes(cells_in_region, my_confidences["items"], my_confidence_threshold)

    post_items_to_collection(my_cell_classes["id"], region_classes)


# mode checking; app supports PREPROCESSING and POSTPROCESSING
mode = get_app_mode()["mode"]
if mode not in ["PREPROCESSING", "POSTPROCESSING"]:
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

modes = {"PREPROCESSING": preprocessing_mode, "POSTPROCESSING": postprocessing_mode}

modes[mode]()

put_finalize()
