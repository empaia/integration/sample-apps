import os
import random
from io import BytesIO
from typing import List, Union

import numpy as np
import requests
from PIL import Image
from scipy.ndimage import binary_fill_holes
from shapely.affinity import scale
from shapely.geometry import Polygon
from skimage.color import rgb2gray
from skimage.filters.thresholding import threshold_otsu
from skimage.measure import find_contours

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

TUMOR_CLASS = "org.empaia.vendor_name.development_app_02.v3.0.classes.tumor"
NON_TUMOR_CLASS = "org.empaia.vendor_name.development_app_02.v3.0.classes.non_tumor"
MAX_GRID_SIZE = 1024
N_HOTSPOTS = 25


class CustomAppException(Exception):
    pass


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def get_overview(my_wsi: dict):
    wsi_id = my_wsi["id"]
    levels = my_wsi["levels"]
    for idx, level in enumerate(levels):
        x_dim = level["extent"]["x"]
        y_dim = level["extent"]["y"]
        if x_dim <= 5000 and y_dim <= 5000:
            scaling = level["downsample_factor"]
            url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{idx}/start/0/0/size/{x_dim}/{y_dim}"
            r = requests.get(url, headers=HEADERS)
            r.raise_for_status()
            return Image.open(BytesIO(r.content)), scaling
    return None, None


def get_tissue(overview: Image, scaling: float):
    np_overview = np.array(overview)
    gray_image = rgb2gray(np_overview)
    thresh = threshold_otsu(gray_image)
    binary = gray_image < thresh
    filled = binary_fill_holes(binary)
    filled = np.transpose(filled)
    contours = find_contours(filled)

    tissue_particles = []
    for contour in contours:
        if len(contour) > 250:
            particle = Polygon(contour)
            particle = scale(particle, xfact=scaling, yfact=scaling, origin=(0, 0))
            tissue_particles.append(particle)

    return tissue_particles


def generate_hotspot_candidates(tissue_particles: List[Polygon]):
    hotspot_candidates = []

    for particle in tissue_particles:
        minx, miny, maxx, maxy = particle.bounds

        x_range = maxx - minx
        y_range = maxy - miny
        grid_size = min(min(x_range / 10, y_range / 10), MAX_GRID_SIZE)

        posy = miny
        while posy < maxy:
            posx = minx
            while posx < maxx:
                rect = Polygon(
                    [
                        (posx, posy),
                        (posx + grid_size, posy),
                        (posx + grid_size, posy + grid_size),
                        (posx, posy + grid_size),
                    ]
                )
                if particle.covers(rect):
                    hotspot_candidates.append(rect)
                posx += grid_size
            posy += grid_size

    return hotspot_candidates


def generate_outputs(hotspot_candidates: List[Polygon], my_wsi: dict):
    npp_created = my_wsi["pixel_size_nm"]["x"]
    hotspots = []
    hotspot_classes = []
    hotspot_ratios = []

    random.seed(0)
    indexes = []
    for _ in range(0, N_HOTSPOTS):
        indexes.append(random.randint(0, len(hotspot_candidates) - 1))

    for i, hsc in enumerate(hotspot_candidates):
        if i not in indexes:
            continue

        upper_left = [int(hsc.exterior.coords[0][0]), int(hsc.exterior.coords[0][1])]
        minx, miny, maxx, maxy = hsc.bounds
        width = int(maxx - minx)
        height = int(maxy - miny)

        hotspot = {
            "name": "hotspot",
            "type": "rectangle",
            "reference_id": wsi_id,
            "reference_type": "wsi",
            "upper_left": upper_left,
            "width": width,
            "height": height,
            "npp_created": npp_created,
            "npp_viewing": [
                npp_created,
                1000000000,
            ],
            "creator_type": "job",
            "creator_id": JOB_ID,
        }
        hotspots.append(hotspot)

        hotspot_class = {
            "value": TUMOR_CLASS if i % 2 == 0 else NON_TUMOR_CLASS,
            "reference_id": None,
            "reference_type": "annotation",
            "type": "class",
            "creator_type": "job",
            "creator_id": JOB_ID,
        }
        hotspot_classes.append(hotspot_class)

        hotspot_ratio = {
            "name": "hotspot_ratio",
            "value": 0.75 if i % 2 == 0 else 0.25,
            "type": "float",
            "creator_type": "job",
            "creator_id": JOB_ID,
            "reference_id": None,
            "reference_type": "annotation",
        }
        hotspot_ratios.append(hotspot_ratio)

    return hotspots, hotspot_classes, hotspot_ratios


mode = get_app_mode()["mode"]

if mode != "PREPROCESSING":
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)


my_wsi = get_input("my_wsi")
wsi_id = my_wsi["id"]

overview, scaling = get_overview(my_wsi)

if not overview or not scaling:
    error_msg = "No tissue detected!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

tissue_particles = get_tissue(overview, scaling)

my_hotspots = {
    "item_type": "rectangle",
    "items": [],
    "type": "collection",
    "creator_type": "job",
    "creator_id": JOB_ID,
}
my_hotspots = post_output("my_hotspots", my_hotspots)

my_hotspot_classes = {
    "item_type": "class",
    "items": [],
    "type": "collection",
    "creator_type": "job",
    "creator_id": JOB_ID,
}
my_hotspot_classes = post_output("my_hotspot_classes", my_hotspot_classes)

my_hotspot_ratios = {
    "item_type": "float",
    "items": [],
    "type": "collection",
    "creator_type": "job",
    "creator_id": JOB_ID,
}
my_hotspot_ratios = post_output("my_hotspot_ratios", my_hotspot_ratios)

hotspot_candidates = generate_hotspot_candidates(tissue_particles)
hotspots, hotspot_classes, hotspot_ratios = generate_outputs(hotspot_candidates, my_wsi)

if len(hotspots) > 0:
    hotspot_items_with_ids = post_items_to_collection(my_hotspots["id"], hotspots)

    for i in range(len(hotspot_items_with_ids["items"])):
        hotspot_classes[i]["reference_id"] = hotspot_items_with_ids["items"][i]["id"]
        hotspot_ratios[i]["reference_id"] = hotspot_items_with_ids["items"][i]["id"]

    post_items_to_collection(my_hotspot_classes["id"], hotspot_classes)
    post_items_to_collection(my_hotspot_ratios["id"], hotspot_ratios)

put_finalize()
