import os
from typing import Union

import requests

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


class CustomAppException(Exception):
    pass


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS, timeout=60)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS, timeout=60)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS, timeout=60)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS, timeout=60)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"
    data = {"user_message": str(error)}
    r = requests.put(url, json=data, headers=HEADERS, timeout=60)
    r.raise_for_status()


mode = get_app_mode()["mode"]

# mode must be preprocessing
if mode != "PREPROCESSING":
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)


my_wsi = get_input("my_wsi")
wsi_id = my_wsi["id"]

my_string = {
    "name": "String Primitive",
    "description": "A string primitive job result",
    "reference_id": wsi_id,
    "reference_type": "wsi",
    "type": "string",
    "value": "very nice WSI",
    "creator_type": "job",
    "creator_id": JOB_ID,
}

post_output("my_string", my_string)

put_finalize()
