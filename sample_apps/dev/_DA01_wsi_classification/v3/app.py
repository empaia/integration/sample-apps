import os
from typing import Union

import requests

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


mode = get_app_mode()["mode"]

if mode != "PREPROCESSING":
    put_failure("Invalid job mode!")

my_wsi = get_input("my_wsi")
wsi_id = my_wsi["id"]

is_tumor = {
    "name": "is_tumor",
    "value": True,
    "type": "bool",
    "creator_type": "job",
    "creator_id": JOB_ID,
    "reference_id": wsi_id,
    "reference_type": "wsi",
}

post_output("is_tumor", is_tumor)

put_finalize()
