import os
from io import BytesIO

import requests
from app.logging_tools import get_logger
from app.request_hooks import check_for_errors_hook, response_logging_hook
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


class ApiInterface:
    def __init__(self, log_level: int):
        self.logger = get_logger(__name__, log_level)

        self.logger.info(f"{APP_API=} {JOB_ID=}")

        self.session = requests.Session()
        hooks = [check_for_errors_hook, response_logging_hook]
        self.session.hooks["response"] = [hook(self.logger) for hook in hooks]

    def get_input(self, key: str) -> dict:
        """
        get input data by key as defined in EAD
        """
        url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
        r = self.session.get(url, headers=HEADERS)

        return r.json()

    def post_output(self, key: str, data: dict) -> dict:
        """
        post output data by key as defined in EAD
        """
        url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
        r = self.session.post(url, json=data, headers=HEADERS)

        return r.json()

    def post_items_to_collection(self, collection_id: str, items: list):
        """
        add items to an existing output collection

        Parameters:
            items: list of data elements
        """
        url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
        r = self.session.post(url, json={"items": items}, headers=HEADERS)
        r.raise_for_status()
        return r.json()

    def get_wsi_tile(self, rectangle: dict, wsi_slide: dict) -> Image.Image:
        """
        get a WSI tile on level 0

        Parameters:
            wsi_slide: contains WSI id (and meta data)
            rectangle: tile position on level 0
        """
        x, y = rectangle["upper_left"]
        width = rectangle["width"]
        height = rectangle["height"]

        wsi_id = wsi_slide["id"]
        level = 0

        tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"

        r = self.session.get(tile_url, headers=HEADERS)

        return Image.open(BytesIO(r.content))

    def put_finalize(self):
        """
        finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
        """
        url = f"{APP_API}/v3/{JOB_ID}/finalize"
        r = self.session.put(url, headers=HEADERS)
