import os
from io import BytesIO
from typing import Union

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def count_tumor_cells(wsi_tile: Image):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return 42


class InputRegionTooSmall(Exception):
    pass


try:
    my_wsi = get_input("my_wsi")
    my_rectangle = get_input("my_rectangle")

    min_rectangle_side = 1024

    for side in ("width", "height"):
        if my_rectangle[side] < min_rectangle_side:
            raise InputRegionTooSmall(f"Input rectangle must have {side} greater than {min_rectangle_side}")

    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

    tumor_cell_count = {
        "name": "cell count tumor",  # choose name freely
        "type": "integer",
        "value": count_tumor_cells(wsi_tile),
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }

    post_output("tumor_cell_count", tumor_cell_count)

    put_finalize()

except Exception as e:
    put_failure(e)
