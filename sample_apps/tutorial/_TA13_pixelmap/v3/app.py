import math
import os
from io import BytesIO
from typing import List, Union

import numpy as np
import requests
from PIL import Image
from scipy.ndimage import binary_fill_holes
from shapely.affinity import scale
from shapely.geometry import Polygon
from skimage.color import rgb2gray, rgb2hed
from skimage.filters.thresholding import threshold_multiotsu, threshold_otsu
from skimage.measure import find_contours

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


TISSUE_CLASS = "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.tissue"
CELL_NUCLEI_CLASS = "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.cell_nuclei"
BACKGROUND_CLASS = "org.empaia.vendor_name.tutorial_app_13.v3.0.classes.background"
PIXELMAP_ELEMENT_TYPE = "uint8"


class CustomAppException(Exception):
    pass


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(wsi_id: str, level: int, tile_x: int, tile_y: int):
    """
    get a WSI tile
    """
    tile_url = f"{APP_API}/v3/{JOB_ID}/tiles/{wsi_id}/level/{level}/position/{tile_x}/{tile_y}"
    params = {"image_format": "png"}
    r = requests.get(tile_url, params=params, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_tile_to_pixelmap(pixelmap_id: str, level: int, tile_x: int, tile_y: int, data: bytes):
    """
    add tile to an existing output pixelmap
    """
    url = f"{APP_API}/v3/{JOB_ID}/pixelmaps/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data"
    r = requests.put(url, data=data, headers=HEADERS)
    r.raise_for_status()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def get_empty_collection(item_type: str):
    """
    create an empty collection
    """
    return {
        "item_type": item_type,
        "items": [],
        "type": "collection",
        "creator_type": "job",
        "creator_id": JOB_ID,
    }


def get_nominal_pixelmap_core(reference_id: str):
    """
    create a nominal pixelmap with core properties
    """
    return {
        "type": "nominal_pixelmap",
        "name": "Segmentation Map",
        "description": "Tissue and cell nuclei segmentation map",
        "reference_type": "wsi",
        "reference_id": reference_id,
        "creator_type": "job",
        "creator_id": JOB_ID,
        "element_type": PIXELMAP_ELEMENT_TYPE,
    }


def get_primitive(name: str, desc: str, item_type: str, value: Union[int, float], reference_id: str):
    """
    create primitive
    """
    return {
        "type": item_type,
        "name": name,
        "description": desc,
        "reference_type": "wsi",
        "reference_id": reference_id,
        "creator_type": "job",
        "creator_id": JOB_ID,
        "value": value,
    }


def get_overview(wsi: dict):
    """
    create an overview image of complete wsi with maximum possible dimensions
    """
    wsi_id = wsi["id"]
    levels = wsi["levels"]
    for idx, level in enumerate(levels):
        x_dim = level["extent"]["x"]
        y_dim = level["extent"]["y"]
        if x_dim <= 5000 and y_dim <= 5000:  # 5000 x 5000 is a contraint from the wsi service
            scaling_factor = level["downsample_factor"]
            url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{idx}/start/0/0/size/{x_dim}/{y_dim}"
            r = requests.get(url, headers=HEADERS)
            r.raise_for_status()
            return Image.open(BytesIO(r.content)), scaling_factor
    return None, None


def get_tissue_and_global_thresholds(overview_image: Image, scaling_factor: float):
    """
    create polygons around tissue particles
    """
    np_overview = np.array(overview_image)
    gray_image = rgb2gray(np_overview)
    thresh = threshold_otsu(gray_image)
    binary = gray_image < thresh
    filled = binary_fill_holes(binary)
    filled = np.transpose(filled)
    contours = find_contours(filled)

    tissue_particle_contours = []
    for contour in contours:
        if len(contour) > 250:
            particle = Polygon(contour)
            particle = scale(particle, xfact=scaling_factor, yfact=scaling_factor, origin=(0, 0))
            tissue_particle_contours.append(particle)

    # get threshold for nuclei segmentation
    h_gray_image = rgb2hed(overview_image)[:, :, 0]
    threshold_nuclei = threshold_multiotsu(h_gray_image, classes=4)[1]

    return tissue_particle_contours, thresh, threshold_nuclei


def get_tile_range(wsi: dict, particles: List[Polygon]):
    min_x = 0
    min_y = 0
    max_x = 0
    max_y = 0

    for particle in particles:
        particle_bounds = particle.bounds
        if particle_bounds[0] < min_x:
            min_x = particle_bounds[0]
        if particle_bounds[1] < min_y:
            min_y = particle_bounds[1]
        if particle_bounds[2] > max_x:
            max_x = particle_bounds[2]
        if particle_bounds[3] > max_y:
            max_y = particle_bounds[3]

    tilesize_x = wsi["tile_extent"]["x"]

    tile_x_min = math.floor(min_x / tilesize_x)
    tile_y_min = math.floor(min_y / tilesize_x)
    tile_x_max = math.ceil(max_x / tilesize_x)
    tile_y_max = math.ceil(max_y / tilesize_x)

    return (tile_x_min, tile_y_min, tile_x_max, tile_y_max)


def calculate_tissue_ratio(particles: List[Polygon], wsi: dict):
    """
    calculate the ratio of tissue to wsi area
    """
    x_dim = wsi["extent"]["x"]
    y_dim = wsi["extent"]["y"]

    wsi_polygon = Polygon(((0, 0), (x_dim, 0), (x_dim, y_dim), (0, y_dim)))

    area_particles = sum([particle.area for particle in particles])
    return (area_particles / wsi_polygon.area) * 100


def get_tissue_particle_polygons(particles: List[Polygon], wsi: dict):
    """
    create polygon annotations for tissue particles
    """
    polygons = []
    for particle in particles:
        polygon = {
            "type": "polygon",
            "name": "Tissue particle",
            "description": "Segemented tissue particle",
            "reference_type": "wsi",
            "reference_id": wsi["id"],
            "creator_type": "job",
            "creator_id": JOB_ID,
            "npp_created": wsi["pixel_size_nm"]["x"],
            "npp_viewing": [
                wsi["pixel_size_nm"]["x"],
                wsi["pixel_size_nm"]["x"] * 1000,  # upper value ensures that polygon is always visible
            ],
            "coordinates": [[int(coord[0]), int(coord[1])] for coord in particle.exterior.coords],
        }
        polygons.append(polygon)
    return polygons


def get_tissue_classes(particles: dict):
    """
    create classes for tissue polygons
    """
    classes = []
    for i in range(len(particles)):
        tissue_class = {
            "value": TISSUE_CLASS,
            "reference_id": particles[i]["id"],
            "reference_type": "annotation",
            "type": "class",
            "creator_type": "job",
            "creator_id": JOB_ID,
        }
        classes.append(tissue_class)
    return classes


def get_tiles_to_process(particles: List[Polygon], wsi: dict):
    """
    return tiles on wsi base layer intersected or covered by any given tissue polygon
    """
    base_level = wsi["levels"][0]
    x_dim = base_level["extent"]["x"]
    y_dim = base_level["extent"]["y"]
    tilesize_x = wsi["tile_extent"]["x"]
    tilesize_y = wsi["tile_extent"]["y"]

    tiles = []

    columns = math.ceil(x_dim / tilesize_x)
    rows = math.ceil(y_dim / tilesize_y)

    for r in range(rows):
        for c in range(columns):
            upper_left_x = c * tilesize_x
            upper_left_y = r * tilesize_y
            new_tile = Polygon(
                (
                    (upper_left_x, upper_left_y),
                    (upper_left_x + tilesize_x, upper_left_y),
                    (upper_left_x + tilesize_x, upper_left_y + tilesize_y),
                    (upper_left_x, upper_left_y + tilesize_y),
                )
            )
            for particle in particles:
                if particle.intersects(new_tile):
                    tiles.append(new_tile)
                    break
    return tiles


def process_tile(wsi_id: str, level: int, tile_x: int, tile_y: int, tissue_threshold: float, nuclei_threshold: float):
    """
    process a wsi base layer tile at given indexes to create pixelmap data for tissue and cell nuclei
    """
    channel_data = []

    tile_image = get_wsi_tile(wsi_id, level, tile_x, tile_y)

    # tissue channel
    np_image = np.array(tile_image)
    gray_image = rgb2gray(np_image)
    binary = gray_image < tissue_threshold
    tissue_data = binary_fill_holes(binary)
    tissue_data = tissue_data * 1
    tissue_data = tissue_data.astype(PIXELMAP_ELEMENT_TYPE)
    channel_data.append(tissue_data.tobytes())

    # nuclei channel
    h_gray_image = rgb2hed(tile_image)[:, :, 0]
    binary_nuclei = h_gray_image > nuclei_threshold
    nuclei_data = binary_nuclei * 2
    nuclei_data = nuclei_data.astype(PIXELMAP_ELEMENT_TYPE)
    channel_data.append(nuclei_data.tobytes())

    return b"".join(channel_data)


mode = get_app_mode()["mode"]

# error if app mode not PREPROCESSING
if mode != "PREPROCESSING":
    error_msg = "Invalid job mode!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# get input wsi
input_wsi = get_input("input_wsi")
input_wsi_id = input_wsi["id"]

# check wsi meta data -> tiles must be a square and between 256 and 2048
tile_extent_x = input_wsi["tile_extent"]["x"]
tile_extent_y = input_wsi["tile_extent"]["y"]

if tile_extent_x != tile_extent_y:
    error_msg = "Only WSIs supported where tilesize_x == tilesize_y!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

if tile_extent_x < 256 or tile_extent_x > 2048:
    error_msg = "Only WSIs supported where 256 <= tilesize <= 2048!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# get overview image and scaling
overview, scaling = get_overview(input_wsi)

# error if no overview
if not overview or not scaling:
    error_msg = "Overview could not be created!"
    put_failure(error_msg)
    raise CustomAppException(error_msg)

# detect tissue -> result: list of polygons with base layer coordinates
tissue_particles, global_tissue_threshold, global_nuclei_threshold = get_tissue_and_global_thresholds(overview, scaling)

particle_collection = get_empty_collection(item_type="polygon")
particle_collection = post_output("particles", particle_collection)

particle_class_collection = get_empty_collection(item_type="class")
particle_class_collection = post_output("particle_classes", particle_class_collection)

particle_polygons = get_tissue_particle_polygons(tissue_particles, input_wsi)

if len(particle_polygons) > 0:
    particle_items_with_ids = post_items_to_collection(particle_collection["id"], particle_polygons)

    tissue_classes = get_tissue_classes(particle_items_with_ids["items"])
    post_items_to_collection(particle_class_collection["id"], tissue_classes)

particle_count = get_primitive(
    name="Number of tissue particles",
    desc="The number of detected tissue particles",
    item_type="integer",
    value=len(particle_polygons),
    reference_id=input_wsi_id,
)

post_output("number_of_tissue_particles", particle_count)

tissue_area_ratio = get_primitive(
    name="Ratio tissue to WSI area (in %)",
    desc="Ratio of detected tissue to WSI area in %",
    item_type="float",
    value=calculate_tissue_ratio(tissue_particles, input_wsi),
    reference_id=input_wsi_id,
)

post_output("tissue_ratio", tissue_area_ratio)

# get bounds of tissue particles
tile_range = get_tile_range(input_wsi, tissue_particles)

# get nominal pixelmap with core properties
pixelmap = get_nominal_pixelmap_core(input_wsi_id)

# set pixelmap meta data
pixelmap["tilesize"] = tile_extent_x  # tilesize equals tilesize of slide
pixelmap["levels"] = [  # pixelmap level: only base level will be created
    {
        "slide_level": 0,
        "position_min_x": tile_range[0],
        "position_min_y": tile_range[1],
        "position_max_x": tile_range[2],
        "position_max_y": tile_range[3],
    }
]
pixelmap["channel_count"] = 2  # channel count: 2 -> tissue and cell nuclei
pixelmap["channel_class_mapping"] = [
    {"number_value": 0, "class_value": TISSUE_CLASS},
    {"number_value": 1, "class_value": CELL_NUCLEI_CLASS},
]
pixelmap["neutral_value"] = 0  # neutral value: 0 -> will be transparent in Generic App UI
pixelmap["element_class_mapping"] = [
    {"number_value": 0, "class_value": BACKGROUND_CLASS},
    {"number_value": 1, "class_value": TISSUE_CLASS},
    {"number_value": 2, "class_value": CELL_NUCLEI_CLASS},
]

pixelmap = post_output("tissue_nuclei_map", pixelmap)

# get base layer tiles completely / partly within particles
tiles_to_process = get_tiles_to_process(tissue_particles, input_wsi)

# process all revelant tiles to create pixelmap data
for tile in tiles_to_process:
    upper_left = tile.exterior.coords[0]
    x = int(upper_left[0] / tile_extent_x)
    y = int(upper_left[1] / tile_extent_y)
    tile_data = process_tile(input_wsi_id, 0, x, y, global_tissue_threshold, global_nuclei_threshold)
    put_tile_to_pixelmap(pixelmap["id"], 0, x, y, tile_data)

put_finalize()
