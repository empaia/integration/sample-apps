import os
from io import BytesIO
from typing import Union

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

# valid class values from ead for classification and tumor score calculation
TUMOR_CLASS = "org.empaia.vendor_name.tutorial_app_12.v3.0.classes.tumor"
NON_TUMOR_CLASS = "org.empaia.vendor_name.tutorial_app_12.v3.0.classes.non_tumor"


def get_app_mode():
    """
    get the used app mode of this job
    """
    url = f"{APP_API}/v3/{JOB_ID}/mode"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str, shallow: bool = False):
    """
    get input data by key as defined in EAD

    Parameters:
        shallow: only return nested collections without lowest level of actual data
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}?shallow={shallow}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, upper_left: dict, width: int, height: int):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        upper_left: upper left coordinate of the tile
        width: width of the tile
        height: height of the tile
    """
    x = upper_left[0]
    y = upper_left[1]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def post_items_to_collection(collection_id: str, items: list):
    """
    add items to an existing output collection

    Parameters:
        items: list of data elements
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    r = requests.post(url, json={"items": items}, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def put_failure(error: Union[str, Exception]):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    data = {"user_message": str(error)}

    r = requests.put(url, json=data, headers=HEADERS)
    r.raise_for_status()


def detect_cells(my_wsi: dict, upper_left: dict, width: int, height: int, n_cells: int):
    """
    pretends to find cells in a rectangle

    Parameters:
        my_wsi: contains WSI id (and meta data)
        upper_left: upper left coordinate of the rectangle in the wsi to search cells in
        width: width of the rectangle in the wsi to search cells in
        height: height of the rectangle in the wsi to search cells in
        n_cells: number of cells to detect
    """
    wsi_tile = get_wsi_tile(my_wsi, upper_left, width, height)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    npp_lower = my_wsi["pixel_size_nm"]["x"]
    npp_upper = npp_lower * my_wsi["levels"][-1]["downsample_factor"]

    cells_batch = []

    for _ in range(n_cells):
        cell = {
            "name": "cell",
            "type": "polygon",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [
                [upper_left[0], upper_left[1]],
                [upper_left[0] + 10, upper_left[1]],
                [upper_left[0] + 10, upper_left[1] + 10],
                [upper_left[0], upper_left[1] + 10],
            ],  # Always use WSI base level coordinates
            "npp_created": npp_lower,
            # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                npp_lower,
                npp_upper,
            ],
            # (optional) recommended pixel reslution range for viewer to display annotation,
            # if npp_created is not sufficient
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }

        cells_batch.append(cell)

    return cells_batch


def processing():
    # for preprocessing mode
    # pre-calculate tiles
    tiles = []
    x_cur = x_min
    while x_cur < x_max:
        y_cur = y_min
        width = TILE_SIZE
        if x_cur + width > x_max:
            width = x_max - x_cur
        while y_cur < y_max:
            height = TILE_SIZE
            if y_cur + height > y_max:
                height = y_max - y_cur
            tiles.append(
                (
                    [x_cur, y_cur],  # upper-left
                    width,
                    height,
                )
            )
            y_cur += height
        x_cur += width

    # prepare and post a shallow collection
    my_cells = {
        "item_type": "polygon",
        "items": [],
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }
    my_cells = post_output("my_cells", my_cells)

    # detect cells per tile
    n_cells = 10  # detect 10 cells per tile
    # to fasten things and testing purpose we only pick 2 tiles:
    n_tiles_median = len(tiles) // 2
    for tile in tiles[n_tiles_median : n_tiles_median + 2]:
        upper_left = tile[0]
        width = tile[1]
        height = tile[2]
        # detect cells with and classes
        cells_batch = detect_cells(my_wsi, upper_left, width, height, n_cells)
        # post cells and receive their ids
        post_items_to_collection(my_cells["id"], cells_batch)

    put_finalize()


def get_cells_in_rectangle(my_rect: dict, input_cells: dict):
    """
    find cells from a given cell collection inside a rectangle

    Parameters:
        my_rect: the rectangle
        input_cells: collection of polygon annotations
    """

    cells_in_rect = []

    min_x = my_rect["upper_left"][0]
    max_x = my_rect["upper_left"][0] + my_rectangle["width"]
    min_y = my_rect["upper_left"][1]
    max_y = my_rect["upper_left"][1] + my_rectangle["height"]

    for cell in input_cells["items"]:
        centroid = cell["centroid"]

        if centroid[0] >= min_x and centroid[0] <= max_x and centroid[1] >= min_y and centroid[1] <= max_y:
            cells_in_rect.append(cell)

    return cells_in_rect


def classify_cells(cells_in_rect):
    """
    pretends to classify cells

    Parameters:
        cells_in_rect: list of polygon annotations
    """

    classes_batch = []
    for i in range(len(cells_in_rect)):
        cell_class = {
            "value": TUMOR_CLASS if i % 2 == 0 else NON_TUMOR_CLASS,  # valid class value from ead
            "reference_id": cells_in_rect[i]["id"],
            "reference_type": "annotation",
            "type": "class",  # NEW required in v3 apps
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        classes_batch.append(cell_class)

    return classes_batch


class InputRegionTooSmall(Exception):
    pass


mode = get_app_mode()["mode"]
my_wsi = get_input("my_wsi")

# we will process the area tiled in 512*512 tiles
TILE_SIZE = 512

# detect cells only in area defined by rectangle
if mode == "PREPROCESSING":
    x_min = 0
    x_max = my_wsi["levels"][0]["extent"]["x"]
    y_min = 0
    y_max = my_wsi["levels"][0]["extent"]["y"]

    processing()

elif mode == "POSTPROCESSING":
    my_rectangle = get_input("my_rectangle")
    my_cells = get_input("my_cells")

    # prepare and post a shallow collection
    my_cell_classes = {
        "item_type": "class",
        "items": [],
        "type": "collection",  # NEW required in v3 apps
        "creator_type": "job",  # NEW required in v3 apps
        "creator_id": JOB_ID,  # NEW required in v3 apps
    }
    my_cell_classes = post_output("my_cell_classes", my_cell_classes)

    # get cells inside my_rectangle
    cells_in_rect = get_cells_in_rectangle(my_rectangle, my_cells)

    tumor_ratio = 0.0

    # only classify cells if cells inside my_rectangle
    if len(cells_in_rect) > 0:
        classes = classify_cells(cells_in_rect)

        # add classes to output collection
        post_items_to_collection(my_cell_classes["id"], classes)

        tumor_count = 0
        non_tumor_count = 0

        # calculate tumor count
        for cl in classes:
            if cl["value"] == TUMOR_CLASS:
                tumor_count += 1
            if cl["value"] == NON_TUMOR_CLASS:
                non_tumor_count += 1

        tumor_ratio = tumor_count / (tumor_count + non_tumor_count)

    # prepare and post a primitive output
    tumor_ratio_output = {
        "name": "Tumor Ratio",
        "description": "Tumor Ratio",
        "reference_id": my_rectangle["id"],
        "reference_type": "annotation",
        "type": "float",
        "value": tumor_ratio,
        "creator_id": JOB_ID,
        "creator_type": "job",
    }
    post_output("tumor_ratio", tumor_ratio_output)

    put_finalize()
