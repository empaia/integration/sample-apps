import os

import requests

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def calculate_tumour_length(my_wsi: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: WSI Image
    """
    return 42


def create_questionnaire_response(my_questionnaire_id: str, tumour_length: str):
    """
    create the FHIR QuestionnaireResponse based on a FHIR Questionnaire.
    my_questionnaire_response refers to a Questionnaire that can e.g. be based on
    an International Collaboration on Cancer Reporting (ICCR) Dataset, in this example:
    ICCR Prostate Cancer - Prostate Core Needle Biopsy (Specimen Level Reporting)

    Parameters:
        my_questionnaire: represents the FHIR Questionnaire
        tumour_length: tumour length that was previously calculated with the WSI
    """

    my_questionnaire_response = {
        "resourceType": "QuestionnaireResponse",
        "questionnaire": f"Questionnaire/{my_questionnaire_id}",
        "status": "in-progress",
        "item": [
            {
                "linkId": "3",
                "text": "TUMOUR EXTENT",
                "item": [
                    {
                        "linkId": "3.2",
                        "text": "Length of tissue involved by carcinoma in mm",
                        "answer": [{"valueDecimal": tumour_length}],
                    }
                ],
            }
        ],
    }

    return my_questionnaire_response


# inputs
my_wsi = get_input("my_wsi")
my_questionnaire = get_input("my_questionnaire")

# calculate tumour length
tumour_length = calculate_tumour_length(my_wsi)

# create QuestionnaireResponse Object
my_questionnaire_response = create_questionnaire_response(my_questionnaire["id"], tumour_length)

# post QuestionnaireResponse Object
post_output("my_questionnaire_response", my_questionnaire_response)

# finalize
put_finalize()
