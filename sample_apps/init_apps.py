from pathlib import Path

import docker

# dev apps v3
import sample_apps.dev._DA01_wsi_classification.v3 as _DA01_wsi_classification_v3
import sample_apps.dev._DA02_hotspot_detection.v3 as _DA02_hotspot_detection_v3
import sample_apps.dev._DA03_region_scores.v3 as _DA03_region_scores_v3
import sample_apps.dev._DA04_confidence_classification.v3 as _DA04_confidence_classification_v3
import sample_apps.dev._DA05_regions_classification.v3 as _DA05_regions_classification_v3
import sample_apps.dev._DA06_simple_preprocessing.v3 as _DA06_simple_preprocessing_v3

# EATS only invalid test apps
import sample_apps.test.eats.invalid_apps._IEA01_wsi_id_used_twice.v3 as _IEA01_wsi_id_used_twice_v3
import sample_apps.test.eats.invalid_apps._IEA02_configuration_invalid.v3 as _IEA02_configuration_invalid_v3

# GPU test app v1 v2 v3
import sample_apps.test.gpu._GPU01_gpu_support_test_app.v1 as _GPU01_gpu_support_test_app_v1
import sample_apps.test.gpu._GPU01_gpu_support_test_app.v2 as _GPU01_gpu_support_test_app_v2
import sample_apps.test.gpu._GPU01_gpu_support_test_app.v3 as _GPU01_gpu_support_test_app_v3

# internal apps v1 v2 v3
import sample_apps.test.internal._ITA01_100k_items.v1 as _ITA01_100k_items_v1
import sample_apps.test.internal._ITA01_100k_items.v2 as _ITA01_100k_items_v2
import sample_apps.test.internal._ITA01_100k_items.v3 as _ITA01_100k_items_v3
import sample_apps.test.internal._ITA02_wsi_collection.v1 as _ITA02_wsi_collection_v1
import sample_apps.test.internal._ITA02_wsi_collection.v2 as _ITA02_wsi_collection_v2
import sample_apps.test.internal._ITA02_wsi_collection.v3 as _ITA02_wsi_collection_v3
import sample_apps.test.internal._ITA03_frontend_testdata_app.v1 as _ITA03_frontend_testdata_app_v1
import sample_apps.test.internal._ITA03_frontend_testdata_app.v2 as _ITA03_frontend_testdata_app_v2
import sample_apps.test.internal._ITA03_frontend_testdata_app.v3 as _ITA03_frontend_testdata_app_v3
import sample_apps.test.internal._ITA04_frontend_testdata_app.v1 as _ITA04_frontend_testdata_app_v1
import sample_apps.test.internal._ITA04_frontend_testdata_app.v2 as _ITA04_frontend_testdata_app_v2
import sample_apps.test.internal._ITA04_frontend_testdata_app.v3 as _ITA04_frontend_testdata_app_v3
import sample_apps.test.internal._ITA05_input_templates.v1 as _ITA05_input_templates_v1
import sample_apps.test.internal._ITA05_input_templates.v2 as _ITA05_input_templates_v2
import sample_apps.test.internal._ITA05_input_templates.v3 as _ITA05_input_templates_v3
import sample_apps.test.internal._ITA06_nested_large_collections.v1 as _ITA06_nested_large_collections_v1
import sample_apps.test.internal._ITA06_nested_large_collections.v2 as _ITA06_nested_large_collections_v2
import sample_apps.test.internal._ITA06_nested_large_collections.v3 as _ITA06_nested_large_collections_v3
import sample_apps.test.internal._ITA07_large_input_collection.v1 as _ITA07_large_input_collection_v1
import sample_apps.test.internal._ITA07_large_input_collection.v2 as _ITA07_large_input_collection_v2
import sample_apps.test.internal._ITA07_large_input_collection.v3 as _ITA07_large_input_collection_v3
import sample_apps.test.internal._ITA08_fluorescence_test_app.v1 as _ITA08_fluorescence_test_app_v1
import sample_apps.test.internal._ITA08_fluorescence_test_app.v2 as _ITA08_fluorescence_test_app_v2
import sample_apps.test.internal._ITA08_fluorescence_test_app.v3 as _ITA08_fluorescence_test_app_v3
import sample_apps.test.internal._ITA09_order_collection_items.v1 as _ITA09_order_collection_items_v1
import sample_apps.test.internal._ITA09_order_collection_items.v2 as _ITA09_order_collection_items_v2
import sample_apps.test.internal._ITA09_order_collection_items.v3 as _ITA09_order_collection_items_v3
import sample_apps.test.internal._ITA10_configuration_tests.v1 as _ITA10_configuration_tests_v1
import sample_apps.test.internal._ITA10_configuration_tests.v2 as _ITA10_configuration_tests_v2
import sample_apps.test.internal._ITA10_configuration_tests.v3 as _ITA10_configuration_tests_v3
import sample_apps.test.internal._ITA11_10_classes.v1 as _ITA11_10_classes_v1
import sample_apps.test.internal._ITA11_10_classes.v2 as _ITA11_10_classes_v2
import sample_apps.test.internal._ITA11_10_classes.v3 as _ITA11_10_classes_v3
import sample_apps.test.internal._ITA12_job_input_class_constraint.v1 as _ITA12_job_input_class_constraint_v1
import sample_apps.test.internal._ITA12_job_input_class_constraint.v2 as _ITA12_job_input_class_constraint_v2
import sample_apps.test.internal._ITA12_job_input_class_constraint.v3 as _ITA12_job_input_class_constraint_v3
import sample_apps.test.internal._ITA13_job_report.v3 as _ITA13_job_report_v3

# JES test app v1 v2 v3
import sample_apps.test.jes._JTA01_app_behaviour.v1 as _JTA01_app_behaviour_v1
import sample_apps.test.jes._JTA01_app_behaviour.v2 as _JTA01_app_behaviour_v2
import sample_apps.test.jes._JTA01_app_behaviour.v3 as _JTA01_app_behaviour_v3

# tutorial apps v1
import sample_apps.test.tutorial_legacy._TA01_simple_app.v1 as _TA01_simple_app_v1

# tutorial apps v2
import sample_apps.test.tutorial_legacy._TA01_simple_app.v2 as _TA01_simple_app_v2
import sample_apps.test.tutorial_legacy._TA02_collections_and_references.v1 as _TA02_collections_and_references_v1
import sample_apps.test.tutorial_legacy._TA02_collections_and_references.v2 as _TA02_collections_and_references_v2
import sample_apps.test.tutorial_legacy._TA03_annotation_results.v1 as _TA03_annotation_results_v1
import sample_apps.test.tutorial_legacy._TA03_annotation_results.v2 as _TA03_annotation_results_v2
import sample_apps.test.tutorial_legacy._TA04_output_references_output.v1 as _TA04_output_references_output_v1
import sample_apps.test.tutorial_legacy._TA04_output_references_output.v2 as _TA04_output_references_output_v2
import sample_apps.test.tutorial_legacy._TA05_classes.v1 as _TA05_classes_v1
import sample_apps.test.tutorial_legacy._TA05_classes.v2 as _TA05_classes_v2
import sample_apps.test.tutorial_legacy._TA06_large_collections.v1 as _TA06_large_collections_v1
import sample_apps.test.tutorial_legacy._TA06_large_collections.v2 as _TA06_large_collections_v2
import sample_apps.test.tutorial_legacy._TA07_configuration.v1 as _TA07_configuration_v1
import sample_apps.test.tutorial_legacy._TA07_configuration.v2 as _TA07_configuration_v2
import sample_apps.test.tutorial_legacy._TA08_failure_endpoint.v1 as _TA08_failure_endpoint_v1
import sample_apps.test.tutorial_legacy._TA08_failure_endpoint.v2 as _TA08_failure_endpoint_v2
import sample_apps.test.tutorial_legacy._TA09_additional_inputs.v1 as _TA09_additional_inputs_v1
import sample_apps.test.tutorial_legacy._TA09_additional_inputs.v2 as _TA09_additional_inputs_v2
import sample_apps.test.tutorial_legacy._TA10_app_with_ui.v2 as _TA10_app_with_ui_v2

# tutorial apps v3
import sample_apps.tutorial._TA01_simple_app.v3 as _TA01_simple_app_v3
import sample_apps.tutorial._TA02_collections_and_references.v3 as _TA02_collections_and_references_v3
import sample_apps.tutorial._TA03_annotation_results.v3 as _TA03_annotation_results_v3
import sample_apps.tutorial._TA04_output_references_output.v3 as _TA04_output_references_output_v3
import sample_apps.tutorial._TA05_classes.v3 as _TA05_classes_v3
import sample_apps.tutorial._TA06_large_collections.v3 as _TA06_large_collections_v3
import sample_apps.tutorial._TA07_configuration.v3 as _TA07_configuration_v3
import sample_apps.tutorial._TA08_failure_endpoint.v3 as _TA08_failure_endpoint_v3
import sample_apps.tutorial._TA09_additional_inputs.v3 as _TA09_additional_inputs_v3
import sample_apps.tutorial._TA10_app_with_ui.v3 as _TA10_app_with_ui_v3
import sample_apps.tutorial._TA11_preprocessing.v3 as _TA11_preprocessing_v3
import sample_apps.tutorial._TA13_pixelmap.v3 as _TA13_pixelmap_v3
import sample_apps.tutorial._TA14_structured_report.v3 as _TA14_structured_report_v3

TUTORIAL_APPS_V1 = [
    _TA01_simple_app_v1,
    _TA02_collections_and_references_v1,
    _TA03_annotation_results_v1,
    _TA04_output_references_output_v1,
    _TA05_classes_v1,
    _TA06_large_collections_v1,
    _TA07_configuration_v1,
    _TA08_failure_endpoint_v1,
    _TA09_additional_inputs_v1,
]

TUTORIAL_APPS_V2 = [
    _TA01_simple_app_v2,
    _TA02_collections_and_references_v2,
    _TA03_annotation_results_v2,
    _TA04_output_references_output_v2,
    _TA05_classes_v2,
    _TA06_large_collections_v2,
    _TA07_configuration_v2,
    _TA08_failure_endpoint_v2,
    _TA09_additional_inputs_v2,
    _TA10_app_with_ui_v2,
]

TUTORIAL_APPS_V3 = [
    _TA01_simple_app_v3,
    _TA02_collections_and_references_v3,
    _TA03_annotation_results_v3,
    _TA04_output_references_output_v3,
    _TA05_classes_v3,
    _TA06_large_collections_v3,
    _TA07_configuration_v3,
    _TA08_failure_endpoint_v3,
    _TA09_additional_inputs_v3,
    _TA10_app_with_ui_v3,
    _TA11_preprocessing_v3,
    _TA13_pixelmap_v3,
    _TA14_structured_report_v3,
]

INTERNAL_APPS_V1 = [
    _ITA01_100k_items_v1,
    _ITA02_wsi_collection_v1,
    _ITA03_frontend_testdata_app_v1,
    _ITA04_frontend_testdata_app_v1,
    _ITA05_input_templates_v1,
    _ITA06_nested_large_collections_v1,
    _ITA07_large_input_collection_v1,
    _ITA08_fluorescence_test_app_v1,
    _ITA09_order_collection_items_v1,
    _ITA10_configuration_tests_v1,
    _ITA11_10_classes_v1,
    _ITA12_job_input_class_constraint_v1,
]

INTERNAL_APPS_V2 = [
    _ITA01_100k_items_v2,
    _ITA02_wsi_collection_v2,
    _ITA03_frontend_testdata_app_v2,
    _ITA04_frontend_testdata_app_v2,
    _ITA05_input_templates_v2,
    _ITA06_nested_large_collections_v2,
    _ITA07_large_input_collection_v2,
    _ITA08_fluorescence_test_app_v2,
    _ITA09_order_collection_items_v2,
    _ITA10_configuration_tests_v2,
    _ITA11_10_classes_v2,
    _ITA12_job_input_class_constraint_v2,
]

INTERNAL_APPS_V3 = [
    _ITA01_100k_items_v3,
    _ITA02_wsi_collection_v3,
    _ITA03_frontend_testdata_app_v3,
    _ITA04_frontend_testdata_app_v3,
    _ITA05_input_templates_v3,
    _ITA06_nested_large_collections_v3,
    _ITA07_large_input_collection_v3,
    _ITA08_fluorescence_test_app_v3,
    _ITA09_order_collection_items_v3,
    _ITA10_configuration_tests_v3,
    _ITA11_10_classes_v3,
    _ITA12_job_input_class_constraint_v3,
    _ITA13_job_report_v3,
]

JES_TEST_APPS = [_JTA01_app_behaviour_v1, _JTA01_app_behaviour_v2, _JTA01_app_behaviour_v3]

GPU_TEST_APPS = [_GPU01_gpu_support_test_app_v1, _GPU01_gpu_support_test_app_v2, _GPU01_gpu_support_test_app_v3]

EATS_INVALID_TEST_APPS = [_IEA01_wsi_id_used_twice_v3, _IEA02_configuration_invalid_v3]

DEV_APPS_V3 = [
    _DA01_wsi_classification_v3,
    _DA02_hotspot_detection_v3,
    _DA03_region_scores_v3,
    _DA04_confidence_classification_v3,
    _DA05_regions_classification_v3,
    _DA06_simple_preprocessing_v3,
]

APPS = []
APPS.extend(TUTORIAL_APPS_V1)
APPS.extend(TUTORIAL_APPS_V2)
APPS.extend(TUTORIAL_APPS_V3)
APPS.extend(INTERNAL_APPS_V1)
APPS.extend(INTERNAL_APPS_V2)
APPS.extend(INTERNAL_APPS_V3)
APPS.extend(JES_TEST_APPS)
APPS.extend(GPU_TEST_APPS)
APPS.extend(EATS_INVALID_TEST_APPS)
APPS.extend(DEV_APPS_V3)


def _build_img(docker_client, path, app):
    print(f"Building Image tagged {app.registry}")
    docker_client.images.build(path=str(path), dockerfile="Dockerfile", tag=app.registry)
    image = docker_client.images.get(app.registry)
    return image


def _push_img(docker_client, app, image):
    image.tag(app.registry)
    print(f"Image tagged to {app.registry}")
    print("Pushing to registry")
    print(docker_client.images.push(app.registry))
    print("Push successfull")


def init_apps():
    client = docker.from_env()

    print("###")
    print("INITIALIZE APPS")
    print(f"TOTAL APP COUNT: {len(APPS)}")
    print("###\n")

    app_counter = 0

    for app in APPS:
        app_counter += 1
        print(f"# APP NO {app_counter}")
        print(f"Checking app: {app.registry}")
        app_dir = Path(app.__file__).parent

        try:
            # pull from registry
            print("Pulling from registry")
            client.images.pull(app.registry)
            print("Image exists in registry")
        except docker.errors.NotFound:
            print("Image does not exist in registry")
            # build
            image = _build_img(client, app_dir, app)
            # push to registry
            _push_img(client, app, image)
