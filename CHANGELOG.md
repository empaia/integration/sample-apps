# Changelog

## 0.2.22

* added codecheck in pipeline

## 0.2.20 - 0.2.21

* added tutorial app 14 for FHIR based structured reports usage to support EMP-0105

## 0.2.19

* removed app ui config from dev apps

## 0.2.18

* added dev apps to init script

## 0.2.17

* added DA02 and DA05 to portal app metadata

## 0.2.15 & 0.2.16

* added tutorial app 13 with pixelmaps
* added portal app metadata

## 0.2.14

* fixes meta_data file for MPS

## 0.2.13

* added generic app ui development apps

## 0.2.12

* added new eats inputs

## 0.2.11

* added sleep to ITA11

## 0.2.10

* added class constraint to input roi for TA11

## 0.2.9

* fix for class namespaces of tutorial app v2/v3

## 0.2.8

* fix for class namespaces of tutorial app v2/v3

## 0.2.7

* fix for tutorial app 11

## 0.2.6

* added tutorial app TA12 for containerized postprocessing

## 0.2.5

* again fix for test app ITA13

## 0.2.4

* fix for test app ITA13

## 0.2.3

* added ITA13 to portal app meta data

## 0.2.2

* added reports app

## 0.2.1

* updated ead for tutorial app 07 v3

## 0.2.0

* restructured repository
* removed unnecessary apps / input folder
* added ead v3 apps
* fixed inconsistencies in app namespaces

## 0.1.3

* added sample_apps/valid/tutorial/_11_preprocessing

## 0.1.2

* restructured sss metadata file

## 0.1.1

* added temporary solution store metadata file for frontend app

## 2021-07-26

* added `sample_apps/meta_data/solution_store_meta_data.json`

## 2021-03-31

* init
